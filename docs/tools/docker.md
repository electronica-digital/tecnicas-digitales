# Docker

## Analogías

* **imagen** -> *clase*
* **contenedor** -> *instancia de una clase*

## Instalación

### Debian

```bash
apt remove docker docker-engine docker.io
apt install apt-transport-https ca-certificates curl gnupg2 software-properties-common 
curl -fsSL https://download.docker.com/linux/debian/gpg | apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian buster stable"
apt update
apt install docker-ce -t buster
```

**nota**: Si no usa *Debian Sid* puede usar el siguiente comando
```bash
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/debian $(lsb_release -cs) stable"
```

### Ubuntu

```bash
apt-get remove docker docker-engine docker.io containerd runc
apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common 
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu $(lsb_release -cs) stable"
apt-get update
apt-get install docker-ce
```

### Linux Mint 19

```bash
apt-get remove docker docker-engine docker.io containerd runc
apt-get install apt-transport-https ca-certificates curl gnupg-agent software-properties-common 
curl -fsSL https://download.docker.com/linux/ubuntu/gpg | sudo apt-key add -
add-apt-repository "deb [arch=amd64] https://download.docker.com/linux/ubuntu bionic stable"
apt-get update
apt-get install docker-ce
```

## Comandos

* `docker run <image>`: Lanzar una imagen
* `docker run --rm <imagen>`: Eliminar el contenedor al salir
* `docker run -it <imagen>`: Abrir una terminal interactiva en el contenedor
* `docker run -p puerto_local:puerto_contenedor -d <imagen>`: Hace un tunel entre el host y el contenedor si el servicio del contendor tiene un puerto que se requiere usar
* `docker run -p puerto_local:puerto_contenedor -d -v <path_archivos_locales> <imagen>`: Se comparten los ficheros locales con el contenedor

## Permisos de usuario a docker

Los siguientes comandos ejecútarlos como SUPERUSUARIO

```bash
groupadd docker
usermod -aG docker $USER
```

## "Dockerizar" una aplicación

Pasos:

1. Tener aplicación desarrollada
2. Crear un *Dockerfile*
3. Construir la imagen
4. Publicar la imagen



