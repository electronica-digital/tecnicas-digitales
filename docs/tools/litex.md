# litex

**Observaciónes**: 

* Tenga en cuenta que en ocaciones puede requerir permisos de SUPERUSUARIO

* Pueda que no sea suficiente con las instrucciones mostradas a continuación; le invito a abrir `issues` para sugerir ayudas en el canal de discord o en github:

[discord: técnicas-digitales](https://discord.gg/ej3EaUq)

[Github repositorio](https://github.com/johnnycubides/litex)

## Paquetes que se instalarán en la imagen

* Litex
* Yosys
* Verilator
* Openocd
* Risc-v toolchain
* Lm-32 toolchain

## Generación de imagen en su PC

Para éste fin hay dos maneras posibles de hacerlo:

1. Construyendo su propia imagen (haciendo uso del **Dockerfile**)
2. Halando la imagen (hacer pull) desde **Docker Hub** (imagen preconstruida)

A continuación se describe cómo **hacer alguna de estas dos operaciones** (no haga las dos!!!):


### 1. Construcción de imagen desde el **Dockerfile**

```bash
git clone https://github.com/johnnycubides/litex.git
cd litex
docker build -t johnnycubides/litex -f Dockerfile .
```

### 2. Pull de imagen preconstruida en **Docker Hub**

Vaya a la pestaña de **custom-tools-docker**

O visite el siguiente link para ver las diferentes opciones: [repositorios docker](/tools/custom-tools-docker/)

Puede darle un vitazo en el [Docker Hub](https://hub.docker.com/search?q=johnnycubides&type=image)

## Instanciar imagen (crear contenedor)

Luego de tener la imagen en su equipo (puede comprobar ésto con `docker images`),
ubíquese en el directorio que usted eligió para trabajar sus proyectos el cual
compartirá con el contenedor y luego haga lo siguiente:

```bash
docker run --privileged -v /dev/bus/usb:/dev/bus/usb -v $(pwd):/home --name litex -it -d johnnycubides/litex
```

**Nota**: Si necesita lanzar aplicaciones que requieran **GUI** puede ejecutar el comando como sigue:

```bash
docker run --privileged --env="DISPLAY" --volume="$HOME/.Xauthority:/root/.Xauthority:rw" --net=host -v /dev/bus/usb:/dev/bus/usb -v $(pwd):/home --name litex -it -d johnnycubides/litex
```

El anterior comando da permisos al uso de hardware conectado a los puertos USB, haciendo que se genere un "tunel"
para que el contenedor puede acceder a cualquier dispositivo conectado a su equipo por ese medio, debe tener encuenta
éste hecho para las operaciones que se realicen.

## Verificación de contendor creado y corriendo

Al ejecutar el siguiente comando debe aparecer en la lista de los contenedores el contenedor
litex corriendo.

```bash
docker container ps
```

Los contenedores que no están iniciados se pueden ver con el siguiente comando

```bash
docker container ps -a
```

## Hacer uso de el bash del contenedor creado

A partir de acá ya puede hacer uso de las herramientas instaladas; haga uso del siguiente
comando:

```bash
docker exec -it litex bash
```

## Por realizar

* Crear un usuario distinto al de root en docker para limitar algunas acciones

* Aún no se ha revisado la conexión con hardware (usb-fpga)

[Dar privilegios para subir el bitstreams](http://www.clifford.at/icestorm/)

[Dar privilegios al contenedor para uso de /dev/*](https://stackoverflow.com/questions/24225647/docker-a-way-to-give-access-to-a-host-usb-or-serial-device)

[Dispositivos USB contenedores](http://marc.merlins.org/perso/linux/post_2018-12-20_Accessing-USB-Devices-In-Docker-_ttyUSB0_-dev-bus-usb-_-for-fastboot_-adb_-without-using-privileged.html)

* Instalación de NextPNR (si es requerido) -> requiere QT5

    * Se requiere revisar las dependencias para correr la construcción del dockerhub automáticamente


