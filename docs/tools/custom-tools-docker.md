# Personalizando herramientas desde dockerhub

Dependiendo las necesidades, cuando se requiera simular, sintetizar o compilar,
pueda que se hagan uso de todas las herramientas presentas en ésta documentación
o solo algunas de ellas. Dependiendo de por ejemplo la FPGA que se cuente o del SoftCore
a usar, puede haber una gran cantidad de recursos a motar innecesariamente.

Para instalar **solo lo que se requiera** en cada caso se presenta la siguiente tabla
de repositorios de imágenes que están alojados en *Docker Hub*

|Docker Hub| Características |Observaciones |
|:-------------:|:-------------:|:-----:|
| [litex-riscv-gc-ice40](#litex-riscv-gc-ice40) |FPGA[ice40], litex, migen, toolchain[lm32, riscv-gc], gtkwave, verilator, openocd[ftdi], nextpnr, icestorm, yosis | Falta hacer el TEST |
| [litex-netxpnr-ice40](#litex-nextpnr-ice40) | FPGA[ice40], litex, migen, toolchain [lm32], gtkwave, verilator, openocd[ftdi], nextpnr, icestorm, yosis | Ok, 12GB aprox |
| [linux-nextpnr-ice40](#linux-nextpnr-ice40) | FPGA[ice40]nextpnr, icestorm, yosis | ok, tamaño aún no determinado| 
| [linux-icestorm-yosis-ice40](#linux-icestorm-yosis-ice40) | FPGA[ice40], icestorm, yosis | ok, tamaño no determinado | 


## Pull de imagen preconstruida

### litex-riscv-gc-ice40

```bash
docker pull johnnycubides/litex-riscv-gc-ice40:litex-riscv-gc-ice40
```

### litex-nextpnr-ice40

```bash
docker pull johnnycubides/litex-nextpnr-ice40:litex-nextpnr-ice40
```

### linux-nextpnr-ice40

```bash
docker pull johnnycubides/linux-nextpnr-ice40:nextpnr-ice40
```

### linux-icestorm-yosis-ice40

```bash
docker pull johnnycubides/linux-icestorm-yosis:icestorm-yosis
```
