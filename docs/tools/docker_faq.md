# FAQ

## Antes tenía acceso a Internet desde el container pero dejo de funcionar

Intente reiniciar el servicio de docker:

```bash
service docker restart
```

## ¿Cómo puedo detener y matar todas las instancias que encuentre de docker?

Recuerde que una instancia de docker es distinta a una imagen, las imagenes
son las que se construyen o se descargan.

Para matar **todas** las instancias (contenedores):

```bash
# Detener los contenedores
docker container stop $(docker container ps -a -q)
docker container prune
```

## Siempre que ejecuto alguna tarea de docker requiere usar SUDO

Agrega el usuario al grupo docker para tal fin, ejemplo:

```bash
groupadd docker
usermod -aG docker $USER
```
