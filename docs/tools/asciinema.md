# Asciinema

<script id="asciicast-264786" src="https://asciinema.org/a/264786.js" async></script>

Herramienta que permite crear vídeos de la terminal,
guardarlos localmente (en la PC), y compartirlos en la nube.

[Oficial Asciinenma]()

## Instalación

[Instalación en distintas plataformas](https://asciinema.org/docs/installation)


### Instlación en Debian

```bash
sudo apt install asciinema
```

## Comandos

```bash
asccinema rec #Inicia vídeo
```

Para terminar vídeo oprima cla tecla *control* simultaneamente con **d** `Ctrl+d`

Para enlazar los vídeos a su cuenta de *asciinema* ejecute el siguiente comando y a continuación visite el siguiente link
```bash
asciinema auth
```

