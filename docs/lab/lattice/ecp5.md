# ECP5

![placa ecp5](../../img/lab/ecp5/ecp5.png)

## Sintetización y programación

![proceso](../../img/lab/ecp5/proceso.png)

Se espera que solo deba ejecutar los dos comandos mostrados arriba en el diagrama; para
tal fin asegúrese de crear y/o modificar los archivos **base.py**, **ecp5-85f.cfg** y 
**load.py** al mismo nivel donde esté trabajando, tenga presente que también debe 
modificar el archivo **trellis.py**. Toda ésta información es explicada a continuación.


## Creación y/o modificación de archivos

### base.py

Modificar el **base.py** teniendo encuenta estas líneas

```py
...
from migen.build.lattice import LatticePlatform
...
("clkE17", 0, Pins("E17"), IOStandard("LVCMOS33")) # Clock 100MHz
...
LatticePlatform.__init__(self, "lfe5um-85f-CABGA756", _io, toolchain="trellis")
...
```

### ecp5-85f.cfg

Crear un archivo con el nombre **ecp5-85f.cfg** y poner el siguiente contenido:

```bash
# ecp3.cfg
# OpenOCD commands

#telnet_port 4444
#gdb_port 3333

interface ftdi
#ftdi_device_desc "FPU1 JTAG Programmer"
ftdi_vid_pid 0x0403 0x6010
ftdi_layout_init 0xfff8 0xfffb
adapter_khz 25000
#adapter_khz 25000

# JTAG TAPs
jtag newtap lfe5u85 tap -expected-id 0x01113043 -irlen 8 -irmask 0xFF -ircapture 0x1
init
scan_chain
svf -tap lfe5u85.tap -quiet -progress build/top.svf
shutdown
```

### load.py

Crear el archivo **load.py** con el siguiente contenido

```py
#!/usr/bin/env python3
import os
os.system("openocd -f ecp5-85f.cfg")
```

### /migen/migen/build/lattice/trellis.py

En el archivo **/migen/migen/build/lattice/trellis.py** modificar la línea **133**

```bash
"nextpnr-ecp5 --json {build_name}.json --lpf {build_name}.lpf --textcfg {build_name}.config --basecfg {basecfg} --{architecture} --freq {freq_constraint}"
```

Con el parámetro **--package CABGA756** quedando el resultado como se muestra a continuación:

```bash
"nextpnr-ecp5 --json {build_name}.json --lpf {build_name}.lpf --textcfg {build_name}.config --basecfg {basecfg} --{architecture} --freq {freq_constraint} --package CABGA756"
```

## ECP5 información de LEDs y SWITCHs

### Headers y User leds

![ecp5_leds](../../img/lab/ecp5/headers_and_leds.png)

#### Pinout J14

![Header](../../img/lab/ecp5/headers.png)


### Switches

![ecp5_switches](../../img/lab/ecp5/switch.png)

## ¿Puedo probar el Blink?

El siguiente script **base.py** permite probar si las herramientas
están cumpliendo con cada uno de sus propósitos.

```py
from migen import *
from migen.build.generic_platform import *
from migen.build.lattice import LatticePlatform

#
# platform
#
_io = [
    ("user_led", 0, Pins("AK29"), IOStandard("LVCMOS25")),
    #("user_dip_btn", 1, Pins("J1"), IOStandard("LVCMOS33")),
    #("user_dip_btn", 2, Pins("H1"), IOStandard("LVCMOS33")),
    ("clkE17", 0, Pins("E17"), IOStandard("LVCMOS33")),
    #("rst_n", 0, Pins("G2"), IOStandard("LVCMOS33")),
]


class Platform(LatticePlatform):
    default_clk_name = "clkE17"
    default_clk_period = 10.0

    def __init__(self, **kwargs):
        LatticePlatform.__init__(self, "lfe5um-85f-caBGA756", _io, toolchain="trellis")

    def do_finalize(self, fragment):
        LatticePlatform.do_finalize(self, fragment)

#
# design
#


# create our platform (fpga interface)
platform = Platform()
led = platform.request("user_led")

# create our module (fpga description)
module = Module()

# create a counter and blink a led
counter = Signal(26)
module.comb += led.eq(counter[24])
module.sync += counter.eq(counter + 1)

#
# build
#
platform.build(module)
```
