from migen import *

from migen.genlib.io import CRG

from litex.build.generic_platform import *
from litex.build.lattice import LatticePlatform

from litex.soc.integration.soc_core import *
from litex.soc.integration.builder import *
from litex.soc.cores import dna, xadc
from litex.soc.cores.spi import SPIMaster

from ios import Led, RGBLed, Button, Switch
from display import SevenSegmentDisplay

#
# platform
#

_io = [
    ("user_led", 0, Pins("B5"), IOStandard("LVCMOS33")),
    ("user_led", 1, Pins("B4"), IOStandard("LVCMOS33")),
    ("user_led", 2, Pins("A2"), IOStandard("LVCMOS33")),
    ("user_led", 3, Pins("A1"), IOStandard("LVCMOS33")),
    ("user_led", 4, Pins("C5"), IOStandard("LVCMOS33")),
    ("user_led", 5, Pins("C4"), IOStandard("LVCMOS33")),
    ("user_led", 6, Pins("B3"), IOStandard("LVCMOS33")),
    ("user_led", 7, Pins("C3"), IOStandard("LVCMOS33")),

    ("cpu_reset", 0, Pins("D16"), IOStandard("LVCMOS33")),
    ("user_sw",  0, Pins("E16"), IOStandard("LVCMOS33")),


    ("serial", 0,
        Subsignal("rx", Pins("B10")),
        Subsignal("tx", Pins("B12"), Misc("PULLUP")),
        Subsignal("rts", Pins("B13"), Misc("PULLUP")),
        Subsignal("cts", Pins("A15"), Misc("PULLUP")),
        Subsignal("dtr", Pins("A16"), Misc("PULLUP")),
        Subsignal("dsr", Pins("B14"), Misc("PULLUP")),
        Subsignal("dcd", Pins("B15"), Misc("PULLUP")),
        IOStandard("LVCMOS33"),
    ),
    
    ("spiflash", 0,
        Subsignal("cs_n", Pins("R12"), IOStandard("LVCMOS33")),
        Subsignal("clk", Pins("R11"), IOStandard("LVCMOS33")),
        Subsignal("mosi", Pins("P12"), IOStandard("LVCMOS33")),
        Subsignal("miso", Pins("P11"), IOStandard("LVCMOS33")),
    ),

    ("clk12", 0, Pins("J3"), IOStandard("LVCMOS33")),
    
    ("user_rgb_led", 0,
        Subsignal("r", Pins("E14")),
        Subsignal("g", Pins("F15")),
        Subsignal("b", Pins("G15")),
        IOStandard("LVCMOS33"),
    ),
]


class Platform(LatticePlatform):
    default_clk_name = "clk12"
    default_clk_period = 83.333

    def __init__(self):
        LatticePlatform.__init__(self, "ice40-hx8k-ct256", _io,
                                 toolchain="icestorm")


def csr_map_update(csr_map, csr_peripherals):
    csr_map.update(dict((n, v)
        for v, n in enumerate(csr_peripherals, start= 8)))


#
# design
#

# create our platform (fpga interface)
platform = Platform()

# create our soc (fpga description)
class BaseSoC(SoCCore):
    # Peripherals CSR declaration
    csr_peripherals = [
        "leds",        
        "switches",
        "rgbled"
    ]

    csr_map_update(SoCCore.csr_map, csr_peripherals)

    def __init__(self, platform):
        sys_clk_freq = int(12e6)
        # SoC with CPU
        SoCCore.__init__(self, platform,
            cpu_type="lm32",
            clk_freq=12e6,
            ident="CPU Test SoC", ident_version=True,
            integrated_rom_size=0x288b,
            integrated_main_ram_size=1024)

        # Clock Reset Generation
        self.submodules.crg = CRG(platform.request("clk12"), platform.request("cpu_reset"))

        # FPGA identification
        

        # FPGA Temperature/Voltage
        

        # Led
        user_leds = Cat(*[platform.request("user_led", i) for i in range(7)])
        self.submodules.leds = Led(user_leds)

        # Switches
        user_switches = Cat(*[platform.request("user_sw", i) for i in range(1)])
        self.submodules.switches = Switch(user_switches)

    

        # RGB Led
        self.submodules.rgbled  = RGBLed(platform.request("user_rgb_led",  0))

        # Accelerometer
        

        # SevenSegmentDisplay
        

soc = BaseSoC(platform)

#
# build
#
builder = Builder(soc, output_dir="build", csr_csv="test/csr.csv")
builder.build()