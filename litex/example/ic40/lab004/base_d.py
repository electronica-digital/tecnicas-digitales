from migen import *

from litex.build.generic_platform import *
from litex.build.lattice import LatticePlatform

from litex.soc.interconnect.csr import *
from litex.soc.interconnect import wishbone 

from litex.soc.integration.soc_core import *
from litex.soc.integration.builder import *

# from litex.soc.cores import dna

from ios import Led, RGBLed, Button, Switch

#
# platform
#

_io = [
    ("user_led",  0, Pins("B5"), IOStandard("LVCMOS33")),
    ("user_led",  1, Pins("B4"), IOStandard("LVCMOS33")),
    ("user_led",  2, Pins("A2"), IOStandard("LVCMOS33")),
    ("user_led",  3, Pins("A1"), IOStandard("LVCMOS33")),

    ("user_sw",  0, Pins("C16"), IOStandard("LVCMOS33")),
    ("user_sw",  1, Pins("B16"), IOStandard("LVCMOS33")),
    ("user_sw",  2, Pins("D16"), IOStandard("LVCMOS33")),
    ("user_sw",  3, Pins("E16"), IOStandard("LVCMOS33")),

    ("user_btn", 0, Pins("F16"), IOStandard("LVCMOS33")),
    ("user_btn", 1, Pins("E14"), IOStandard("LVCMOS33")),
    ("user_btn", 2, Pins("G16"), IOStandard("LVCMOS33")),
    ("user_btn", 3, Pins("F15"), IOStandard("LVCMOS33")),

    ("user_rgb_led", 0,
        Subsignal("r", Pins("C4")),
        Subsignal("g", Pins("B3")),
        Subsignal("b", Pins("C3")),
        IOStandard("LVCMOS33"),
    ),


    ("clk125", 0, Pins("J3"), IOStandard("LVCMOS33")),

    ("cpu_reset", 0, Pins("B2"), IOStandard("LVCMOS33")),

    ("serial", 0,
        Subsignal("tx", Pins("B10")),
        Subsignal("rx", Pins("B12")),
        IOStandard("LVCMOS33"),
    ),


]


class Platform(LatticePlatform):
    default_clk_name = "clk125"
    default_clk_period = 8.0

    def __init__(self):
        LatticePlatform.__init__(self, "ice40-hx8k-ct256", _io, toolchain="icestorm")

    def do_finalize(self, fragment):
        LatticePlatform.do_finalize(self, fragment)


#
# design
#

# create our platform (fpga interface)
platform = Platform()

# create our soc (fpga description)
class BaseSoC(SoCCore):
    # Peripherals CSR declaration
    csr_map = { 
        # "dna":      20, 
        "rgbled":   12, 
        "leds":     13 
        # "switches": 24, 
        # "buttons":  25  
    }   
    csr_map.update(SoCCore.csr_map)

    # print(mem_map = SoCCore.mem_map)
    print(SoCCore.mem_map)
    # rom, sram, csr, main_ram
    # SoCCore.mem_map["main_ram"] =   0x00000000
    # SoCCore.mem_map["csr"] =        0x40000000
    # SoCCore.mem_map["sram"] =       0x41000000
    # SoCCore.mem_map["rom"] =        0x41110000
    SoCCore.mem_map["main_ram"] =   0x00000000
    SoCCore.mem_map["csr"] =        0x10000000
    SoCCore.mem_map["sram"] =       0x11000000
    SoCCore.mem_map["rom"] =        0x11110000

    def __init__(self, platform):
        sys_clk_freq = int(12e6)
        # SoC with CPU
        SoCCore.__init__(self, platform, sys_clk_freq, 
            cpu_type="lm32",
            # cpu_type="picorv32",
            #cpu_type="vexriscv",
            ident="CPU Test SoC", ident_version=True,
            # integrated_rom_size=0x8000,
            integrated_rom_size=10000,
            # integrated_rom_size=8000,
            # integrated_rom_size=12880,
            # integrated_rom_size=15360,
            # integrated_main_ram_size=16*1024)
            integrated_main_ram_size=0,
            # integrated_sram_size=512)
            )

        # Clock Reset Generation
        self.submodules.crg = CRG(platform.request("clk125"), ~platform.request("cpu_reset"))

        # FPGA identification
        # self.submodules.dna = dna.DNA()

        # Led
        user_leds = Cat(*[platform.request("user_led", i) for i in range(4)])
        self.submodules.leds = Led(user_leds)

        # Switches
        # user_switches = Cat(*[platform.request("user_sw", i) for i in range(4)])
        # self.submodules.switches = Switch(user_switches)

        # Buttons
        # user_buttons = Cat(*[platform.request("user_btn", i) for i in range(4)])
        # self.submodules.buttons = Button(user_buttons)

        # RGB Led
        # self.submodules.rgbled  = RGBLed(platform.request("user_rgb_led",  0))

soc = BaseSoC(platform)

#
# build
#
builder = Builder(soc, output_dir="build", csr_csv="test/csr.csv")
builder.build()
