from migen import *
from migen.build.generic_platform import *
from migen.build.lattice import LatticePlatform

#
# platform
#
_io = [
    ("user_led", 0, Pins("AK29"), IOStandard("LVCMOS25")),
    #("user_dip_btn", 1, Pins("J1"), IOStandard("LVCMOS33")),
    #("user_dip_btn", 2, Pins("H1"), IOStandard("LVCMOS33")),
    ("clkE17", 0, Pins("E17"), IOStandard("LVCMOS33")),
    #("rst_n", 0, Pins("G2"), IOStandard("LVCMOS33")),
]


class Platform(LatticePlatform):
    default_clk_name = "clkE17"
    default_clk_period = 10.0

    def __init__(self, **kwargs):
        #LatticePlatform.__init__(self, "LFE5UM5G-85F-8BG381", _io, toolchain="trellis")
        #LatticePlatform.__init__(self, "LFE5UM-85F-8BG756", _io, toolchain="trellis")
        LatticePlatform.__init__(self, "lfe5um-85f-caBGA756", _io, toolchain="trellis")
        #LatticePlatform.__init__(self, "LFE5UM-85F-CABGA756", _io, toolchain="trellis")

    def do_finalize(self, fragment):
        LatticePlatform.do_finalize(self, fragment)

#
# design
#	


# create our platform (fpga interface)
platform = Platform()
led = platform.request("user_led")

# create our module (fpga description)
module = Module()

# create a counter and blink a led
counter = Signal(26)
module.comb += led.eq(counter[24])
module.sync += counter.eq(counter + 1)

#
# build
#
platform.build(module)
