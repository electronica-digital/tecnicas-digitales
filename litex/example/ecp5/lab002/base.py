from migen import *
from migen.build.generic_platform import *
from migen.build.lattice import LatticePlatform
from migen.genlib.cdc import MultiReg

from tick import *
from display import *
from bcd import *
from core import *
# from bcd import _BCD


#
# platform
#

_io = [
    ("user_led",  0, Pins("AG30"), IOStandard("LVCMOS33")),

    ("user_sw",  0, Pins("C26"), IOStandard("LVCMOS33")),

    ("user_btn_r", 0, Pins("D26"), IOStandard("LVCMOS33")),
    ("user_btn_l", 0, Pins("A28"), IOStandard("LVCMOS33")),

    ("clk100", 0, Pins("E17"), IOStandard("LVCMOS33")),

    ("cpu_reset", 0, Pins("A29"), IOStandard("LVCMOS33")),

    ("display_cs_n",  0, Pins("AL32 AK29 AK30 AH32 AG32 AJ29 AM28 AJ30"), IOStandard("LVCMOS33")),
    ("display_abcdefg",  0, Pins("AK32 AJ32 AM30 AL30 AK31 AJ31 AM31"), IOStandard("LVCMOS33")),
]


class Platform(LatticePlatform):
    default_clk_name = "clk100"
    default_clk_period = 10.0

    def __init__(self):
        LatticePlatform.__init__(self, "lfe5um-85f-CABGA756", _io, toolchain="trellis")

    def do_finalize(self, fragment):
        LatticePlatform.do_finalize(self, fragment)

#
# design
#

# user button detection
class UserButtonPress(Module):
    def __init__(self, user_btn):
        self.rising = Signal()

        # # #

        _user_btn = Signal()
        _user_btn_d = Signal()

        # resynchronize user_btn
        self.specials += MultiReg(user_btn, _user_btn)
        # detect rising edge
        self.sync += [
            _user_btn_d.eq(user_btn),
            self.rising.eq(_user_btn & ~_user_btn_d)
        ]

# create our platform (fpga interface)
platform = Platform()

# create our main module (fpga description)
class Clock(Module):
    sys_clk_freq = int(100e6)
    def __init__(self):
        # Tick generation : timebase
        tick = Tick(self.sys_clk_freq, 1)
        self.submodules += tick

        # SevenSegmentDisplay
        display = SevenSegmentDisplay(self.sys_clk_freq)
        self.submodules += display

        # Core : counts ss/mm/hh
        core = Core()
        self.submodules += core

        # set mm/hh
        btn0_press = UserButtonPress(platform.request("user_btn_r"))
        btn1_press = UserButtonPress(platform.request("user_btn_l"))
        self.submodules += btn0_press, btn1_press

        # Binary Coded Decimal: convert ss/mm/hh to decimal values
        bcd_seconds = BCD()
        bcd_minutes = BCD()
        bcd_hours = BCD()
        self.submodules += bcd_seconds, bcd_minutes, bcd_hours
        # use the generated verilog file
        platform.add_source("bcd.v")

        # combinatorial assignement
        self.comb += [
            # Connect tick to core (core timebase)
            core.tick.eq(tick.ce),

            # Set minutes/hours
            core.inc_minutes.eq(btn0_press.rising),
            core.inc_hours.eq(btn1_press.rising),

            # Convert core seconds to bcd and connect
            # to display
            bcd_seconds.value.eq(core.seconds),
            display.values[0].eq(bcd_seconds.ones),
            display.values[1].eq(bcd_seconds.tens),

            # Convert core minutes to bcd and connect
            # to display
            bcd_minutes.value.eq(core.minutes),
            display.values[2].eq(bcd_minutes.ones),
            display.values[3].eq(bcd_minutes.tens),

            # Convert core hours to bcd and connect
            # to display
            bcd_hours.value.eq(core.hours),
            display.values[4].eq(bcd_hours.ones),
            display.values[5].eq(bcd_hours.tens),

            # Connect display to pads
            platform.request("display_cs_n").eq(~display.cs),
            platform.request("display_abcdefg").eq(display.abcdefg)
        ]

module = Clock()

#
# build
#

platform.build(module)
