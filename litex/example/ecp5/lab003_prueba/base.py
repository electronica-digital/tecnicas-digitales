from migen import *

from migen.build.generic_platform import *
from migen.build.lattice import LatticePlatform


from litex.soc.integration.soc_core import *

from litex.soc.integration.builder import *
from litex.soc.cores.uart import UARTWishboneBridge

from ios import Led, RGBLed, Button, Switch
from display import SevenSegmentDisplay


#
# platform
#

_io = [

    ("user_led",  0, Pins("AG30"), IOStandard("LVCMOS33")),
    ("user_led",  1, Pins("AK29"), IOStandard("LVCMOS33")),
    ("user_led",  2, Pins("AK30"), IOStandard("LVCMOS33")),
    ("user_led",  3, Pins("AH32"), IOStandard("LVCMOS33")),
    ("user_led",  4, Pins("AG32"), IOStandard("LVCMOS33")),
    ("user_led",  5, Pins("AJ29"), IOStandard("LVCMOS33")),
    ("user_led",  6, Pins("AM28"), IOStandard("LVCMOS33")),

    ("user_sw",  0, Pins("C26"), IOStandard("LVCMOS33")),

    ("clk100", 0, Pins("E17"), IOStandard("LVCMOS33")),

    ("cpu_reset", 0, Pins("A29"), IOStandard("LVCMOS33")),

    ("display_cs_n",  0, Pins("AL32 AG28 AG29 AJ28 AH28 AH30 AJ30  "), IOStandard("LVCMOS33")),
    ("display_abcdefg",  0, Pins("AK32 AJ32 AM30 AL30 AK31 AJ31 AM29"), IOStandard("LVCMOS33")),


    ("serial", 0,
        Subsignal("tx", Pins("AK28")),
        Subsignal("rx", Pins("AL28")),
        IOStandard("LVCMOS33"),
    ),


]


class Platform(LatticePlatform):
    default_clk_name = "clk100"
    default_clk_period = 12.0 #ERROR: Max frequency for clock '$glbnet$clk100': 96.67 MHz (FAIL at 100.00 MHz)

    def __init__(self):
        LatticePlatform.__init__(self, "lfe5um-85f-CABGA756", _io, toolchain="trellis")

    def do_finalize(self, fragment):
        LatticePlatform.do_finalize(self, fragment)

#
# design
#

def csr_map_update(csr_map, csr_peripherals):
    csr_map.update(dict((n, v)
        for v, n in enumerate(csr_peripherals, start= 8)))

# create our platform (fpga interface)


# create our soc (fpga description)

class BaseSoC(SoCCore):
    # Peripherals CSR declaration
    csr_peripherals = {
        "leds",
        "display",
        "switches"
    }
    csr_map_update(SoCCore.csr_map, csr_peripherals)

    def __init__(self, platform, sys_clk_freq=166*1000000):
        clk_freq = int((1/(platform.default_clk_period))*1000000000)
        SoCCore.__init__(self, platform, sys_clk_freq,
            cpu_type=None,
            csr_data_width=32,
            with_uart=False,
            ident="LiteEth Base Design",
            with_timer=False
        )

        # Clock Reset Generation
        self.submodules.crg = CRG(platform.request("clk100"), ~platform.request("cpu_reset"))

        # No CPU, use Serial to control Wishbone bus

        self.submodules.uart_bridge = UARTWishboneBridge(platform.request("serial"), sys_clk_freq, baudrate=115200)
        self.add_wb_master(self.uart_bridge.wishbone)

        # Led
        user_leds = Cat(*[platform.request("user_led", i) for i in range(7)])
        self.submodules.leds = Led(user_leds)

        # Switches
        user_switches = Cat(*[platform.request("user_sw", i) for i in range(1)])
        self.submodules.switches = Switch(user_switches)
    
        # SevenSegmentDisplay
        self.submodules.display = SevenSegmentDisplay(sys_clk_freq)
        self.comb += [
            platform.request("display_cs_n").eq(~self.display.cs),
            platform.request("display_abcdefg").eq(~self.display.abcdefg)
        ]

platform = Platform()
soc = BaseSoC(platform)


#
# build
#
builder = Builder(soc, output_dir="build", csr_csv="test/csr.csv")
vns = builder.build(build_name="soc")


